<?php

return [
    'products' => [
        '1000_credits' => env('ONE_THOUSAND_CREDITS_PRODUCT'),
        '500_credits' => env('FIFTY_HUNDRED_CREDITS_PRODUCT'),
        '100_credits' => env('ONE_HUNDRED_CREDITS_PRODUCT'),
    ]
];
