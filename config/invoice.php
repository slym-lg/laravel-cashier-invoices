<?php

return [
    'tax_rate' => env('INVOICE_TAX_RATE'),
    'path' => storage_path('app/public/invoices/'),
];
