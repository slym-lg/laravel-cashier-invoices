<?php

use App\Events\StoreCreditEvent;
use App\Models\Invoice;
use App\Services\Invoices\PDFService;
use Illuminate\Support\Facades\Route;

/**
 * Welcome
 */
Route::get('/', function () {
    return view('welcome');
});

/**
 * Stripe Products routes
 */
Route::get('/product-checkout/1000', function () {
    return auth()->user()->checkout([config('stripe.products.1000_credits') => 1], [
        'success_url' => route('checkout.1000.success'),
        'cancel_url' => route('checkout.cancel'),
    ]);
})->name('product-checkout.1000');

Route::get('/product-checkout/500', function () {
    return auth()->user()->checkout([config('stripe.products.500_credits') => 1], [
        'success_url' => route('checkout.500.success'),
        'cancel_url' => route('checkout.cancel'),
    ]);
})->name('product-checkout.500');

Route::get('/product-checkout/100', function () {
    return auth()->user()->checkout([config('stripe.products.100_credits')  => 1], [
        'success_url' => route('checkout.100.success'),
        'cancel_url' => route('checkout.cancel'),
    ]);
})->name('product-checkout.100');

/**
 * Stripe routes
 */
Route::get('/success/product-checkout/1000', function () {
    StoreCreditEvent::dispatch(1000, 700);
    return to_route('dashboard');
})->name('checkout.1000.success');

Route::get('/success/product-checkout/500', function () {
    StoreCreditEvent::dispatch(500, 400);
    return to_route('dashboard');
})->name('checkout.500.success');

Route::get('/success/product-checkout/100', function () {
    StoreCreditEvent::dispatch(100, 100);
    return to_route('dashboard');
})->name('checkout.100.success');

Route::get('/home', fn () => 'Cancel! :(')->name('checkout.cancel');

/**
 * Invoices routes
 */
Route::get('/invoices/download/{invoice_id}', function () {
    return PDFService::make(
        view: 'pdf.invoice',
        data: [
            'invoice' => Invoice::find(request()->invoice_id),
            'user' => auth()->user(),
        ],
    )
        ->download();
})
    ->name('invoices.download');

/**
 * Dashboard
 */
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

/**
 * Auth
 */
require __DIR__.'/auth.php';
