<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h1>Vous avez {{ auth()->user()->credits }} crédits!</h1>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 flex flex-col bg-white border-b border-gray-200">
                    <a href="{{ route('product-checkout.100') }}">Acheter 100 crédits</a>
                    <a href="{{ route('product-checkout.500') }}">Acheter 500 crédits</a>
                    <a href="{{ route('product-checkout.1000') }}">Acheter 1.000 crédits</a>
                </div>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 flex flex-col bg-white border-b border-gray-200">
                    @forelse(auth()->user()->invoices as $invoice)
                        <a href="{{ route('invoices.download', $invoice->id) }}">Télécharger la facture {{ $invoice->id }}</a>
                    @empty
                        Aucune facture
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
