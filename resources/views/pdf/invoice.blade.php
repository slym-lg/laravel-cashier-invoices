<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Facture {{ $invoice->id }}</title>

    <style>
        html {
            margin-top: 40px !important;
            margin-bottom: 60px !important;
            padding-top: 0 !important;
        }

        body {
            background: #fff none;
            font-family: "Poppins", sans-serif!important;
            margin-top: 0 !important;
            padding-top: 0 !important;
            color: #2D3954;
        }

        table tr th {
            text-align: left;
            font-size: 14px;
            font-weight: bold;
        }

        tbody.content--infos tr:nth-child(even) {
            background: #e8e8e8;

        }

        tbody.content--infos tr.no--bg {
            background: transparent;
        }

        table tr td {
            padding: 10px 10px;
            font-size: 13px;
            color: #808080;

        }

        .container {
            padding-top: 0px;
        }

        .informations--pdf {
            margin: 3px 0;
            padding: 3px;
            border-bottom: 1px solid #e4e8f3;
        }

        .informations--pdf:nth-last-child(1) {
            border-bottom: none;
        }

        h2 {
            width: auto;
            margin: 0;
        }

        .w-full {
            width: 100%;
        }

        .w-50 {
            width: 50%;
        }

        .font-bold {
            font-size: 14px;
            color:#727272;
        }
        .font-price {
            font-weight: bold;
        }
        .text-right {
            text-align: right;
        }

        .text-small {
            font-size: 0.8em;
            font-style: italic;
        }


        .logo--pdf  img {
            width: auto;
            height: 40px;
            object-fit: contain;
        }


        .hideextra {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }

        .pdf--header {
            width: 100%;
            justify-content: space-between;
        }

        .pdf--header .header--right {
            float: right;
            text-align: right;
        }
        .bold-title{
            font-weight: bold;
            padding-bottom: 4px;
        }

        .total--general {
            margin-top: 20px;
            padding: 10px 0;
            border-top: 1px solid #e4e8f3;
        }

        .total--general p {
            margin-bottom: 3px;
            margin-top: 0px;
        }

        .img--label {
            width: 60px;
            height: auto;
            object-fit: contain;
        }
        .w-full thead{
            text-align: left;
            height: 50px;
            background: #FBB03B;
            color: white;
            padding: 30px 0;
        }
        .w-full thead tr th{
            padding: 10px 10px;
        }
    </style>
</head>
<body>

<div class="container">
    <table class="pdf--header">
        <tr class="header--left">
            <td class="w-50 logo--pdf">
                <img  src="#" alt="logo"/>
            </td>
            <td class="w-50">
                <h2 class="text-right">Facture {{ $invoice->id }}</h2>
            </td>
        </tr>
        <tr class="pdf--header">
            <td class="w-50">
                <strong>Isodepot</strong><br/>
                BCE/TVA : BE0000 000 000<br/>
                Avenue des champs 16<br/>
                4000 Liege<br/><br/>

                info@isodepot.be<br/>
                0490/00.00.00
            </td>
            <td class="w-50 header--right">
                <span class="font-bold bold-title">Date de facturation:</span> {{ $invoice->created_at->format('d-m-Y') }}
                <br/>
                <span class="font-bold bold-title">Commande:</span> {{ $invoice->id }}
                <br/>
                <span class="font-bold bold-title">Date de commande:</span> {{ $invoice->created_at->format('d-m-Y') }}<br/><br/>
                <br/>

                <span class="font-bold bold-title">
                    {{ $user->name }}
                </span>

{{--                @if($order->corporation_vat_number)--}}
{{--                    <br/>--}}
{{--                    BCE/TVA: {{ $order->corporation_vat_number }}--}}
{{--                @endif--}}

{{--                <br/>--}}
{{--                <span class="font-bold bold-title">--}}
{{--                        Type de livraison :--}}
{{--                </span>--}}
{{--                <br/>--}}
{{--                @foreach($order->shippingMethods as $shippingMethod)--}}
{{--                    {{ $shippingMethod->pivot->label }}--}}
{{--                @endforeach--}}

{{--                @if($order->shippingMethods->first()->id !== 1)--}}
{{--                    <br/>--}}
{{--                    <span class="font-bold bold-title">--}}
{{--                        Adresse de livraison :--}}
{{--                    </span>--}}
{{--                    <br/>--}}
{{--                    {{ $order->addressShipping->getAddressLine() }}--}}
{{--                @endif--}}

{{--                <br/>--}}
{{--                <span class="font-bold bold-title">--}}
{{--                    Adresse de facturation :--}}
{{--                </span>--}}
{{--                <br/>--}}
{{--                {{ $order->addressBilling->exists ? $order->addressBilling->getAddressLine() : $order->addressShipping->getAddressLine() }}--}}
            </td>
        </tr>
    </table>
    <br/><br/><br/>
    <div class="informations--pdf">
        <table class="w-full">
            <thead>
            <tr>

                <th>
                    Total Taxe
                </th>

                <th>
                    % Taxe
                </th>

                <th>
                    Prix unitaire
                </th>

                <th>
                    Total HTVA
                </th>

            </tr>
            </thead>
            <tbody class="content--infos">
            @foreach($invoice->invoiceItems as $invoiceItem)
                <tr>
                    <td>
                        {{ \App\money($invoiceItem->total_tax) }}
                    </td>

                    <td>
                        {{ $invoiceItem->tax_rate }} %
                    </td>

                    <td>
                        {{ \App\money($invoiceItem->unit_price) }}
                    </td>

                    <td class="price--column">
                        <div class="hideextra" style="width:80px">
                            {{ \App\money($invoiceItem->total_htva) }}
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-right total--general">
            <p>
                <span class="font-bold">Produits HTVA:</span>
                <span class="font-bold font-price">{{ \App\money($invoice->total_sub) }}</span>
            </p>

{{--            @if($order->total_shipping)--}}
{{--                <p>--}}
{{--                    <span class="font-bold">Total Taxes:</span>--}}
{{--                    <span class="font-bold font-price">{{ $invoice->total_tax }}</span>--}}
{{--                </p>--}}
{{--            @endif--}}

            <p>
                <span class="font-bold">TVA (21%):</span>
                <span class="font-bold font-price">{{ \App\money($invoice->total_tax) }}</span>
            </p>

{{--            @if($order->total_discount > 0)--}}
{{--                <p>--}}
{{--                    <span class="font-bold">Réduction:</span>--}}
{{--                    <span class="font-bold font-price">{{  money($order->total_discount)  }}</span>--}}
{{--                </p>--}}
{{--            @endif--}}

            <p>
                <span class="font-bold">Total TVAC:</span>
                <span class="font-bold font-price">{{ \App\money($invoice->total_ttc) }}</span>
            </p>
        </div>
    </div>
</div>
<footer>
</footer>
</body>
</html>
