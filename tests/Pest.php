<?php

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Foundation\Testing\RefreshDatabase;

uses(Tests\TestCase::class, RefreshDatabase::class)->in('Feature');

function actingAs(Authenticatable $user, string $driver = null)
{
    return test()->actingAs($user, $driver);
}
