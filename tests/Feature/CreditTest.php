<?php

use App\Events\StoreCreditEvent;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\User;
use function Pest\Laravel\get;

it('adds the right credits amount to user', function () {
    actingAs($user = User::factory()->create());
    expect($user->credits)->toEqual(0);
    Event::fake();

    get('/success/product-checkout/500');

//    StoreCreditEvent::dispatch(500);
    Event::assertDispatched(StoreCreditEvent::class);

//    expect($user->credits)->toEqual(500);
});

it('stores user invoices with invoice_items for 100 credits', function () {
    actingAs($user = User::factory()->create());

    expect($user->credits)->toEqual(0);
    expect($user->invoices->all())->toHaveCount(0);
    expect(InvoiceItem::all())->toHaveCount(0);

    get('/success/product-checkout/100')
        ->assertSessionHasNoErrors();

    expect($user->credits)->toEqual(100);

    expect(Invoice::first()->user_id)->toEqual($user->id);
    expect(Invoice::first()->total_sub)->toEqual(100 * 100);
    expect(Invoice::first()->total_tax)->toEqual(21 * 100);
    expect(Invoice::first()->total_ttc)->toEqual(121 * 100);

    expect(InvoiceItem::first()->invoice_id)->toEqual(Invoice::first()->id);
    expect(InvoiceItem::first()->total_htva)->toEqual(100 * 100);
    expect(InvoiceItem::first()->total_tax)->toEqual(21 * 100);
    expect(InvoiceItem::first()->tax_rate)->toEqual(config('invoice.tax_rate'));
    expect(InvoiceItem::first()->unit_price)->toEqual(100 * 100);
});

it('stores user invoices with invoice_items for 500 credits', function () {
    actingAs($user = User::factory()->create());

    expect($user->credits)->toEqual(0);
    expect($user->invoices->all())->toHaveCount(0);
    expect(InvoiceItem::all())->toHaveCount(0);

    get('/success/product-checkout/500')
        ->assertSessionHasNoErrors();

    expect($user->credits)->toEqual(500);

    expect(Invoice::first()->user_id)->toEqual($user->id);
    expect(Invoice::first()->total_sub)->toEqual(400 * 100);
    expect(Invoice::first()->total_tax)->toEqual(84 * 100);
    expect(Invoice::first()->total_ttc)->toEqual(484 * 100);

    expect(InvoiceItem::first()->invoice_id)->toEqual(Invoice::first()->id);
    expect(InvoiceItem::first()->total_htva)->toEqual(400 * 100);
    expect(InvoiceItem::first()->total_tax)->toEqual(84 * 100);
    expect(InvoiceItem::first()->tax_rate)->toEqual(config('invoice.tax_rate'));
    expect(InvoiceItem::first()->unit_price)->toEqual(400 * 100);
});

it('stores user invoices with invoice_items for 1000 credits', function () {
    actingAs($user = User::factory()->create());

    expect($user->credits)->toEqual(0);
    expect($user->invoices->all())->toHaveCount(0);
    expect(InvoiceItem::all())->toHaveCount(0);

    get('/success/product-checkout/1000')
        ->assertSessionHasNoErrors();

    expect($user->credits)->toEqual(1000);

    expect(Invoice::first()->user_id)->toEqual($user->id);
    expect(Invoice::first()->total_sub)->toEqual(700 * 100);
    expect(Invoice::first()->total_tax)->toEqual(147 * 100);
    expect(Invoice::first()->total_ttc)->toEqual(847 * 100);

    expect(InvoiceItem::first()->invoice_id)->toEqual(Invoice::first()->id);
    expect(InvoiceItem::first()->total_htva)->toEqual(700 * 100);
    expect(InvoiceItem::first()->total_tax)->toEqual(147 * 100);
    expect(InvoiceItem::first()->tax_rate)->toEqual(config('invoice.tax_rate'));
    expect(InvoiceItem::first()->unit_price)->toEqual(700 * 100);
});
