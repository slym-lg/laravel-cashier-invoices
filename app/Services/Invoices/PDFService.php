<?php

declare(strict_types=1);

namespace App\Services\Invoices;

use Barryvdh\DomPDF\Facade\Pdf;
use Barryvdh\DomPDF\PDF as DomPDF;
use Illuminate\Http\Response;

class PDFService
{
    public function __construct(
        readonly private DomPDF|Pdf $pdf,
    ){}

    public static function make(string $view, array $data): static
    {
        $pdf = Pdf::loadView($view, $data);

        return new static(
            $pdf,
        );
    }

    public function generate(): self
    {
        if (!file_exists(storage_path('app/public/invoices/'))) mkdir(storage_path('app/public/invoices/'));

        $this->pdf->save(
            $this->generatePath(),
        );

        return $this;
    }

    public function download(): Response
    {
        return $this->pdf->download();
    }

    private function generatePath(): string
    {
        return config('invoice.path') . 'I' . str_pad(uniqid(), 7, '0', STR_PAD_LEFT) . '.pdf';
    }
}

