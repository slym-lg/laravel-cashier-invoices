<?php

declare(strict_types=1);

namespace App\Services\Invoices;

class InvoiceService
{
    readonly public int $price;

    public function __construct(int $price)
    {
        $this->price = $price * 100;
    }

    public function calculateTax(): int
    {
        return (int) ( $this->price * (config('invoice.tax_rate') / 100) );
    }

    public function calculateTTC(): int
    {
        return $this->calculateTax() + $this->price;
    }
}
