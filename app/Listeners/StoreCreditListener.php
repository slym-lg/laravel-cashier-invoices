<?php

namespace App\Listeners;

use App\Events\StoreCreditEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreCreditListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StoreCreditEvent  $event
     * @return void
     */
    public function handle(StoreCreditEvent $event)
    {
        auth()->user()->credits += $event->amount;
        auth()->user()->save();
    }
}
