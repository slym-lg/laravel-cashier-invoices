<?php

namespace App\Listeners;

use App\Events\StoreCreditEvent;
use App\Services\Invoices\PDFService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class GenerateInvoiceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StoreCreditEvent  $event
     * @return void
     */
    public function handle(StoreCreditEvent $event)
    {
        PDFService::make(
            view: 'pdf.invoice',
            data: [
                'invoice' => auth()->user()->lastInvoice(),
                'user' => auth()->user(),
            ],
        )
            ->generate();
    }
}
