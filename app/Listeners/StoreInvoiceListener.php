<?php

namespace App\Listeners;

use App\Events\StoreCreditEvent;
use App\Models\InvoiceItem;
use App\Services\Invoices\InvoiceService;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreInvoiceListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\StoreCreditEvent  $event
     * @return void
     */
    public function handle(StoreCreditEvent $event)
    {
        $service = new InvoiceService($event->price);

        auth()->user()->invoices()->create([
            'total_sub' => $service->price,
            'total_tax' => $service->calculateTax(),
            'total_ttc' => $service->calculateTTC()
        ]);

        InvoiceItem::create([
            'total_htva' => $service->price,
            'total_tax' => $service->calculateTax(),
            'tax_rate' => config('invoice.tax_rate'),
            'unit_price' => $service->price,
            'invoice_id' => auth()->user()->invoices()->latest()->first()->id,
        ]);
    }
}
