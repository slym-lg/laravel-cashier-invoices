<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'total_htva',
        'total_tax',
        'tax_rate',
        'unit_price',
        'invoice_id',
    ];

    protected $casts = [
        'total_htva' => 'integer',
        'total_tax' => 'integer',
        'unit_price' => 'integer',
    ];

}
