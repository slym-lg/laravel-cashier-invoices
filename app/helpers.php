<?php

declare(strict_types=1);

namespace App;

use Brick\Money\Money;
use NumberFormatter;

function money($amount, $minDecimal = 0): string
{
    $amount = $amount / 100;

    if (empty($amount)) {
        $amount = 0;
    }

    $formatter = new NumberFormatter('fr_FR', NumberFormatter::CURRENCY);
    $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, $minDecimal);
    $formatter->setSymbol(NumberFormatter::MONETARY_GROUPING_SEPARATOR_SYMBOL, '.');

    $str = Money::of(round($amount, 2), 'EUR')
        ->formatWith($formatter);

    return str_replace(' ', ' ', $str);
}
